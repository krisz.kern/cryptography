base64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'


def hex2bin(s):
    """
    >>> hex2bin('f')
    '1111'
    >>> hex2bin('5')
    '101'
    >>> hex2bin('1')
    '1'
    """
    return bin(int(s, 16))[2:]


def bin2hex(b):
    """
    >>> bin2hex('1111')
    'f'
    >>> bin2hex('100001')
    '21'
    >>> bin2hex('1')
    '1'
    """
    return hex(int(b, 2))[2:]


def fillupbyte(b):
    """
    >>> fillupbyte('011')
    '00000011'
    >>> fillupbyte('1')
    '00000001'
    >>> fillupbyte('10111')
    '00010111'
    >>> fillupbyte('11100111')
    '11100111'
    >>> fillupbyte('111001111')
    '0000000111001111'
    """
    return b.rjust(8 * ((len(b) + 7) // 8), '0')


def int2base64(i):
    """
    >>> int2base64(0x61)
    'YQ=='
    >>> int2base64(0x78)
    'eA=='
    >>> int2base64(3456)
    'DYA=='
    """
    b = hex2bin(hex(i))
    b = fillupbyte(b)
    b = b.ljust(6 * ((len(b) + 5) // 6), '0')
    return ''.join([base64chars[int(fillupbyte(b[i:i + 6]), 2)] for i in range(0, len(b), 6)]) + '=='


def hex2base64(h):
    """
    >>> hex2base64('61')
    'YQ=='
    >>> hex2base64('123456789abcde')
    'EjRWeJq83g=='
    """
    h = h[2:] if h[:2] == '0x' else h
    return int2base64(int(h, 16))

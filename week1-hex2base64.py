import base64


def hex2base64(hex_str):
    hex_str = hex_str[2:] if hex_str[:2] == '0x' else hex_str
    return repr(base64.b64encode(bytearray.fromhex(hex_str)))[2:-1]


print(hex2base64('0x12312f34'))

import random

lst = random.sample(range(0x41, 0x5a), 20)
print(lst)

lst2 = [chr(x) for x in lst]
print(lst2)

lst2_str = ''.join(lst2)
print(lst2_str)

lst2_str_lc = lst2_str.lower()
print(lst2_str_lc)

lst3 = lst2_str_lc[2:10] * 2
print(lst3)

def hex2string(h):
    """
    >>> hex2string('61')
    'a'
    >>> hex2string('776f726c64')
    'world'
    >>> hex2string('68656c6c6f')
    'hello'
    """
    return ''.join([chr(int(h[x:x+2], 16)) for x in range(0, len(h), 2)])


def string2hex(s):
    """
    >>> string2hex('a')
    '61'
    >>> string2hex('hello')
    '68656c6c6f'
    >>> string2hex('world')
    '776f726c64'
    >>> string2hex('foo')
    '666f6f'
    """
    return ''.join([hex(ord(x))[2:] for x in s])


def hex_xor(a, b):
    """
    >>> hex_xor('aabbf11','12345678')
    '189fe969'
    >>> hex_xor('12cc','12cc')
    '0000'
    >>> hex_xor('1234','2345')
    '3171'
    >>> hex_xor('111','248')
    '359'
    >>> hex_xor('8888888','1234567')
    '9abcdef'
    """
    return hex(int(a, 16) ^ int(b, 16))[2:].zfill(max(len(a), len(b)))


def encrypt_single_byte_xor(h, k):
    """
    >>> encrypt_single_byte_xor('aaabbccc','00')
    'aaabbccc'
    >>> encrypt_single_byte_xor(string2hex('hello'),'aa')
    'c2cfc6c6c5'
    >>> hex2string(encrypt_single_byte_xor(encrypt_single_byte_xor(string2hex('hello'),'aa'),'aa'))
    'hello'
    >>> hex2string(encrypt_single_byte_xor(encrypt_single_byte_xor(string2hex('Encrypt and decrypt are the same'),'aa'),'aa'))
    'Encrypt and decrypt are the same'
    """
    return ''.join([hex(int(h[x:x+2], 16) ^ int(k, 16))[2:] for x in range(0, len(h), 2)])


encrypted = 'e9c88081f8ced481c9c0d7c481c7ced4cfc581ccc480'

for i in range(256):
    key = hex(i)[2:].zfill(2)
    print(key)
    decrypted = hex2string(encrypt_single_byte_xor(encrypted, key))
    print(decrypted)
    print('---')

# key is 'a1'
# original text: Hi! You have found me!

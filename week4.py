def hex2string(h):
    """
    >>> hex2string('61')
    'a'
    >>> hex2string('776f726c64')
    'world'
    >>> hex2string('68656c6c6f')
    'hello'
    """
    return ''.join([chr(int(h[x:x+2], 16)) for x in range(0, len(h), 2)])


def string2hex(s):
    """
    >>> string2hex('a')
    '61'
    >>> string2hex('hello')
    '68656c6c6f'
    >>> string2hex('world')
    '776f726c64'
    >>> string2hex('foo')
    '666f6f'
    """
    return ''.join([hex(ord(x))[2:] for x in s])


def encrypt_by_add_mod(txt, k):
    """
    >>> encrypt_by_add_mod('Hello',123)
    'Ãàççê'
    >>> encrypt_by_add_mod(encrypt_by_add_mod('Hello',123),133)
    'Hello'
    >>> encrypt_by_add_mod(encrypt_by_add_mod('Cryptography',10),246)
    'Cryptography'
    """
    h = string2hex(txt)
    return hex2string(''.join([hex((int(h[x:x+2], 16) + k) % 256)[2:] for x in range(0, len(h), 2)]))


def encrypt_xor_with_changing_key_by_prev_cipher(txt, k, mode='encrypt'):
    """
    >>> encrypt_xor_with_changing_key_by_prev_cipher('Hello',123,'encrypt')
    '3V:V9'
    >>> encrypt_xor_with_changing_key_by_prev_cipher(encrypt_xor_with_changing_key_by_prev_cipher('Hello',123,'encrypt'),123,'decrypt')
    'Hello'
    >>> encrypt_xor_with_changing_key_by_prev_cipher(encrypt_xor_with_changing_key_by_prev_cipher('Cryptography',10,'encrypt'),10,'decrypt')
    'Cryptography'
    """
    h = string2hex(txt)
    result = ''
    for x in range(0, len(h), 2):
        current_byte = int(h[x:x+2], 16)
        next_byte = current_byte ^ k
        result += hex(next_byte)[2:]
        if mode == 'encrypt':
            k = next_byte
        else:
            k = current_byte
    return hex2string(result)
